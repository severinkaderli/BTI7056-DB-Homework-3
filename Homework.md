---
title: "BTI7056-DB"
subtitle: "Homework 3"
author:
    - Severin Kaderli
    - Marius Schär
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Dr. Kai Brünnler"
lang: "de-CH"
toc: false
header-includes: |
    \usepackage[]{algorithm2e}
    \usepackage{setspace}
    \doublespacing
    \renewcommand{\arraystretch}{0.7}
    \def\code#1{\texttt{#1}}
...

# Aufgabe 1
> | person | place | date     |
> | ------ | ----- | -------- |
> | alice  | bar   | 1.1.2017 |
> | bob    | bar   | 2.2.2017 |
> | bob    | cafe  | 3.3.2017 |
> 1. Bestimmen Sie alle superkeys fur die gegebene Relation.
> 2. Bestimmen Sie alle candidate keys fur die gegebene Relation.
> 3. Bestimmen Sie alle superkeys fur das gegebene Relationsschema.
> 4. Bestimmen Sie alle candidate keys fur das gegebene Relationsschema.
> 5. Welchen primary key wurden Sie wählen und warum?

1. $\{\text{person}, \text{place}\}$,  
   $\{\text{place}, \text{date}\}$,  
   $\{\text{date}\}$,  
   $\{\text{person}, \text{place}, \text{date}\}$,  
   $\{\text{person}, \text{date}\}$
2. $\{\text{date}\}$,  
   $\{\text{person}, \text{place}\}$
3. Ohne keine weitere Informationen können keine Superkeys bestimmt werden.
4. Ohne keine weitere Informationen können keine Candidate Keys
   bestimmt werden.
5. Einen neuen Surrogate Key definieren.

# Aufgabe 2
> | TelefonNr     | Ort         | Skigebiet     | Lift      | Kapazität |
> | ------------- | ----------- | ------------- | --------- | --------- |
> | 033 854 12 12 | Grindelwald | First         | Oberjoch  | 2500      |
> | 033 854 12 12 | Grindelwald | First         | Oberläger | 2000      |
> | 033 854 12 12 | Grindelwald | Kl. Scheidegg | Fallboden | 3000      |
> | 033 854 12 14 | Wengen      | Kl. Scheidegg | Fallboden | 3000      |
> 1. Bestimmen Sie alle superkeys fur die gegebene Relation.
> 2. Bestimmen Sie alle candidate keys fur die gegebene Relation.
> 3. Welche dieser candidate keys sind auch candidate keys fur das gegebene Schema?
> 4. Welchen primary key würden Sie wählen und warum?

1. $\{\text{TelefonNr}, \text{Lift}\}$,  
   $\{\text{TelefonNr}, \text{Kapazität}\}$,  
   $\{\text{Ort}, \text{Lift}\}$,  
   $\{\text{Ort}, \text{Kapazität}\}$,  
   Alle Superkeys die eine Obermenge der oben genannten Superkeys sind.
2. $\{\text{TelefonNr}, \text{Lift}\}$,  
   $\{\text{TelefonNr}, \text{Kapazität}\}$,  
   $\{\text{Ort}, \text{Lift}\}$,  
   $\{\text{Ort}, \text{Kapazität}\}$
3. $\{\text{Ort}, \text{Lift}\}$,  
   $\{\text{TelefonNr}, \text{Lift}\}$
4. Einen neuen Surrogate Key definieren.  

# Aufgabe 3
> Mengen werden häufig mit Hilfe von Listen implementiert. Geben Sie in einer
> Programmiersprache ihrer Wahl oder in Pseudocode eine Funktion `boolean
> equals(list l1,list l2)` an, die genau dann `true` liefert, wenn die Mengen der
> Elemente der beiden Listen gleich sind. Gegeben sind die Funktionen `int
> head(list l)` und `list tail(list l)`, die fur nichtleere Listen jeweils das
> erste Element und die Restliste liefern, sowie die Funktion `boolean empty(list
> l)`. Geben Sie möglichst einfachen, kurzen und offensichtlich korrekten Code an,
> die Laufzeit spielt keine Rolle. Definieren Sie sich geeignete Hilfsfunktionen.

```{.java .numberLines}
boolean equals(List<E> l1, List<E> l2){
    return allMatch(l1, e -> in(e, l2)) &&
           allMatch(l2, e -> in(e, l1));
}

private boolean allMatch(List<E> iterable, Predicate<E> predicate){
    if(head(iterable) == null){
        return true;
    }
    if (predicate.test(head(iterable))){
        return allMatch(tail(iterable), predicate);
    }
    return false;
}

private boolean in(E e, List<E> list){
    if(head(list) == null){
        return false;
    }
    if(head(list).equals(e)){
        return true;
    }
    return in(e, tail(list));
}

private E head(List<E> list){
    if(list.size() < 1){
        return null;
    }
    return list.get(0);
}

private List<E> tail(List<E> list){
    if(list.size() == 1){
        return Collections.emptyList();
    }
    return list.subList(1, list.size());
}
```

# Aufgabe 4
> Laden Sie die gegebene Datenbank `university.db` mit SQLite.
> 
> 1. Schauen Sie sich das Schema an: `.tables`
> 2. Schauen Sie sich die Instanz (Daten in jeder Tabellen) an:  
>    `SELECT * FROM <table-name>`
> 3. Vergleichen Sie mit dem in der Vorlesung dazu gegebenen Schema. Verstehen
> Sie die Bedeutung der Datensätze in diesen Tabellen, die Primary Keys und die
> Foreign-Key-Beziehungen zwischen den Tabellen.
> 
> Beantworten Sie insbesondere folgende Fragen:

## Frage (a)
> Wieviele `instructors` kann ein `student` als `advisor`haben? Warum?

Ein `student` kann nur einen `instructor` als `advisor` haben, da in der
`advisor` Tabelle die ID des Studenten als Primary Key hinterlegt ist.

## Frage (b)
> (b) Wieviele `students` kann ein `instructor` als `advisor` haben? Warum?

Ein `instructor` als `advisor` kann beliebig viele `students` haben, da
`instructors` in der `advisor` Tabelle mehrmals vorkommen können.

## Frage (c)
> (c) Warum gibt es keine Foreign-Key-Beziehung von `section` auf `time slot`?

Weil `time_slot_id` nicht der Primary Key von der `time_slot` Tabelle ist.